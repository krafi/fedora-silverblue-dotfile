# Greeting
#echo "Welcome to Fedora"

# Prompt
PROMPT="%F{red}┌[%f%F{cyan}%m%f%F{red}]─[%f%F{yellow}%D{%H:%M-%d/%m}%f%F{red}]─[%f%F{magenta}%d%f%F{red}]%f"$'\n'"%F{red}└╼%f%F{green}$USER%f%F{yellow}$%f"

# Export PATH$
export PATH=~/.local/bin:/snap/bin:/usr/sandbox/:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/usr/share/games:/usr/local/sbin:/usr/sbin:/sbin:$PATH
export PATH=~/.npm-global/bin:$HOME/.local/bin:/snap/bin:/usr/sandbox/:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/usr/share/games:/usr/local/sbin:/usr/sbin:/sbin


#####################################################
# Auto completion / suggestion -> Fish-like suggestion for command history
source ~/.local/custom/zsh-autosuggestions/zsh-autosuggestions.zsh

#####source ~/zsh-autocomplete/zsh-autocomplete.plugin.zsh
source ~/.oh-my-zsh/mini-oh-my-zsh.sh

# Fish like syntax highlighting
source ~/.local/custom/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Save type history for completion and easier life
HISTFILE=~/.zsh_history
HISTSIZE=99999
SAVEHIST=99999
setopt appendhistory

# Select all suggestion instead of top on result only
zstyle ':autocomplete:tab:*' insert-unambiguous yes
zstyle ':autocomplete:tab:*' widget-style menu-select
zstyle ':autocomplete:*' min-input 2


#======================================================================================================================
#======================================Star ship will do that================================================================
#======================================================================================================================

# Useful alias for benchmarking programs
# require install package "time" sudo apt install time
# alias time="/usr/bin/time -f '\t%E real,\t%U user,\t%S sys,\t%K amem,\t%M mmem'"

# Display last command interminal
# echo -en "\e]2;Fedora\a"
# preexec () { print -Pn "\e]0;$1 - Fedora\a" }
#======================================================================================================================
#======================================================================================================================
#======================================================================================================================


# alias
alias cudatext='/home/kar18/.local/custom/cudatext-linux-qt5-amd64-1.182.2.0/cudatext'
alias ls='ls --color=auto'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias ram="~/./swapf"
alias addup='git add -u'
alias addall='git add .'
alias branch='git branch'
alias checkout='git checkout'
alias clone='git clone'
alias commit='git commit -m'
alias fetch='git fetch'
alias pull='git pull origin'
alias push='git push origin'
alias stat='git status'  # 'status' is protected name so using 'stat' instead
alias tag='git tag'
alias newtag='git tag -a'
alias jctl="journalctl -p 3 -xb"
#zplug "dracula/zsh", as:theme
#force_color_prompt = yes
alias darkmpv=cat " mpv --vf=negate --hwdec=no "
function b(){
pkexec echo "$1" > /sys/class/backlight/acpi_video0/brightness
}
function hex-encode()
{
  echo "$@" | xxd -p
}

function hex-decode()
{
  echo "$@" | xxd -p -r
}

function rot13()
{
  echo "$@" | tr 'A-Za-z' 'N-ZA-Mn-za-m'
}

function by() {
	echo "gcc compiling your c++...
	"
	g++ "$1" &&
	       	./a.out &&
	echo "------EOL-------"
}
function py(){
	echo "SOL" &&
	py "$1" &&
		echo "EOL"

}
function dnfu(){
	sudo dnf update --refresh

}
function prizrak_zakar(){
cd ~/web/backend-krafiinfo.gitlab.io/HERE/prizrak/ &&
git add . &&
git commit -m "update" &&
git push
}

function prizrak(){
cd ~/web/backend-krafiinfo.gitlab.io/HERE/prizrak/ &&
ghost restart &&
ghost ls
}
function lg() {
    git add .
    git commit -m "$1"
    git push
            
}
function xs() {
    sudo -S /opt/lampp/lampp start
}

function xr() {
    sudo -S /opt/lampp/lampp reload
    #echo ricm | sudo -S /opt/lampp/lampp reload
}

function xss() {
    sudo -S /opt/lampp/lampp stop
}

function 20m() {
   sleep 20m  && espeak -s 150 "sir your 20 min is over" && xcowsay "20 min over" && 20m
}
function tc(){
sudo timeshift --create
}
function webb(){
cd ~/web/backend-of-kazi-ar-rafi.com &&

echo "./static-web-converter.sh 2368" &&

echo "./feature-image-location-fixer.sh" &&

echo "./gitpush.sh"
}

function fireoff(){
 systemctl stop firewalld &&
 sudo ufw allow 1714:1764/udp &&
 sudo ufw reload

}

function paste() {
              local file=${1:-/dev/stdin}
              curl --data-binary @${file} https://paste.rs
}
alias pb="nc termbin.com 9999"

function awei(){
bluetoothctl connect 45:A6:63:27:9F:2F

echo "power on  |  agent on  |  list"
echo "devices    | default-agent    |    trust A0:E9:AF:10:62:3F    |    pair A0:E9:AF:10:62:3F    |    connect A0:E9:AF:10:62:3F "
}
function enc() {
#!/bin/bash

# Display a dialog box to prompt the user for the password
PASS=$(zenity --password --title="Enter Password")  # Change: Added password dialog box

# Check if the user canceled the dialog box
if [[ $? -ne 0 ]]; then
  echo "Password input canceled"
  exit 1
fi

# Continue with the rest of the script using the $PASS variable
# instead of the password input argument

# some color
c0="${reset}${bold}${red}"
c1="${reset}${green}"

echo "${c1} example : ./exc.sh anyfoler your-strong-password"
echo "${c0} "
# Set the  file name
NAME=$1
BACKUP_FILE="$(date +%Y-%m-%d_%H:%M:%S)"

# Create the output directory if it doesn't exist
mkdir -p enc_out

# Set the starting number for the suffix
NUM=0

# Loop until a file name is found that doesn't exist
while [[ -e "${NAME}${NUM}.tar.enc" ]]; do
  ((NUM++))
done

# Create the encrypted file using the new file name
sudo tar -cz "$NAME" | openssl enc -aes-256-cbc -pbkdf2 -e -k "${PASS}" > enc_out/"${NAME}_${NUM}__${BACKUP_FILE}.tar.enc" -v

# Print the name of the backup file
echo "  ${NAME}_${NUM}__${BACKUP_FILE}.tar.enc"

# Print a success message and exit with status code 0
echo "${c0} Congratulation! This file was successfully produced. "
echo "${c1} Keep following. www.krafi.info www.krafi.xyz"

read -n1 -r -p "Press any key to continue..." key

# Print a failure message and exit with non-zero status code
echo "${c0} Oops, something went wrong. Please try again. "
echo "${c1} Check if you entered the correct folder name and password."
echo "${c1} ! TRY AGAIN ! example : ./exc.sh anyfoler your-strong-password"
exit 1
}
dec ()
{
#!/bin/bash
# This script is already added to my .zshrc, I don't need to run this script anymore.

# Example: ./dec.sh any.tar.enc

# Some colors
c0="${reset}${bold}${red}"
c1="${reset}${green}"

echo "${c1}Example: ./dec.sh any.tar.enc"
echo "${c0} "

# Set the filename
NAME=$1
BACKUP_FILE="$(date +%Y-%m-%d_%H:%M:%S)"
NUM=0

# Loop until a filename is found that doesn't exist
while [[ -e "${NAME}${NUM}.tar" ]]; do
  ((NUM++))
done

# Prompt the user for the password
PASS=$(zenity --password --title="Enter Password" --width=300)

# Create the decrypted file using the new filename
openssl enc -aes-256-cbc -pbkdf2 -d -in "${NAME}" -k "${PASS}" | tar -xz

# Print the name of the backup file
echo "${NAME}_${NUM}__${BACKUP_FILE}.tar"

# Print a success message and exit with status code 0
echo "${c0}Congratulations! This file was successfully decrypted."
echo "${c1}Keep following: www.krafi.info www.krafi.xyz"

read -n1 -r -p "Press any key to continue..." key

# Print a failure message and exit with non-zero status code
echo "${c0}Oops, something went wrong. Please try again."
echo "${c1}Check if you entered the correct file name and password."
exit 1

}

ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}


fastfetch
eval "$(starship init zsh)"
