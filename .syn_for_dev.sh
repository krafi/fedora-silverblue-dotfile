#do not run
#run at your own risk
cp -rf /usr/share/fonts/* ./usr/share/fonts/ &&
cp -rf /usr/share/icons/* ./usr/share/icons/ &&
cp -rf /usr/share/themes/* ./usr/share/themes/ &&
cp -rf ~/.gtkrc-2.0 ./dotfile &&
cp -rf ~/.oh-my-zsh ./dotfile  &&
cp -rf ~/.themes ./dotfile  &&
cp -rf ~/.zshrc ./dotfile   &&
cp -rf ~/.config/dunst ./dotfile/.config/ &&
cp -rf ~/.config/feh ./dotfile/.config/ &&
cp -rf ~/.config/gtk-3.0 ./dotfile/.config/ &&
#cp -rf ~/.config/gtk-4.0 ./dotfile/.config/ &&
cp -rf ~/.config/guake ./dotfile/.config/ && #maybe manually
cp -rf ~/.config/i3 ./dotfile/.config/ &&
cp -rf ~/.config/i3status ./dotfile/.config/ &&
cp -rf ~/.config/rofi ./dotfile/.config/ &&
cp -rf ~/.config/sxhkd ./dotfile/.config/ &&
cp -rf ~/.config/tilda ./dotfile/.config/ &&
cp -rf ~/.config/volumeicon ./dotfile/.config/ &&
cp ~/.Xresources ./dotfile &&
cp ~/.config/starship.toml ./dotfile/.config/ &&
cp -rf  ~/.local/custom/ ./dotfile/.local
#cp -rf ~/.config/ ./dotfile/.config/
#cp -rf ~/.config/ ./dotfile/.config/

